﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour {

    public Animator anim;

    private AudioManager _audioManager;

    public bool idleCountdown;
    public bool stepWater = false;

    public float idleTimer;
    public float idleDelay;

	void Awake () {
        anim = GetComponent<Animator>();
        _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        idleTimer = idleDelay;
        idleCountdown = true;
    }

    public void SetAnimatorParameters(string boolName, string status)
    {
        bool trueOrFalse;
        if (status == "true")
            trueOrFalse = true;
        else
            trueOrFalse = false;
        anim.SetBool(boolName, trueOrFalse);
    }

    public void Jump()
    {
        _audioManager.Play("Jump");
    }

    public void Footstep()
    {
        if (stepWater)
            _audioManager.Play("WaterStep");
        else
            _audioManager.Play("Footstep");
    }

    public void Land()
    {
        if (stepWater)
            _audioManager.Play("WaterLand");
        else
            _audioManager.Play("Land");
    }

    public void ColourChange()
    {
        _audioManager.Play("ColourChange");
    }

    void Update ()
    {       
        if (idleCountdown)
        {
            idleTimer = idleTimer - Time.deltaTime;

            if (idleTimer <= 0)
            {
                anim.SetTrigger("OccasionalIdle");
                idleTimer = idleDelay;
            }
        }
    }
}
