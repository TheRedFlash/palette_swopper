﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTurnOffOn : ColourButton {

    public GameObject waterSpray;
    public GameObject waterSpray2;
    public GameObject particles;
    public GameObject particles2;

    private AudioManager _audioManager;

	public override void Start () {
        base.Start();
        _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();

    }

    void Update () {
        if (buttonActivated == true)
        {
            waterSpray.SetActive(false);
            waterSpray2.SetActive(false);
            particles.SetActive(false);
            particles2.SetActive(false);
            _audioManager.Stop("FallingWater");
        }
	}
}
