﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Block Colour inherits from PlayerColour as it will share a lot of the functionality
public class BlockColour : PlayerColour {

    public string startingColour;

    public override void Start()
    {
        rend = GetComponent<Renderer>();
        if (!Array.Exists(availableColours, element => element == startingColour))
            Debug.LogWarning(name + " is missing a valid starting colour!");
        currentColour = startingColour;
        ChangeMaterial();
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Water")
        {
            currentColour = "Grey";
            ChangeMaterial();
        }
    }
}
