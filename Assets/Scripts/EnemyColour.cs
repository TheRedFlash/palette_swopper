﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyColour : PlayerColour {

	// Use this for initialization
	public override void Start () {
        rend = transform.Find("Clabers_Mesh").GetComponent<Renderer>();
        currentColour = tag;
        if (Array.Exists(availableColours, element => element == tag))
            ChangeMaterial();
        else
            Debug.LogWarning("The following enemy has an invalid colour entry - " + name);       
	}
}
