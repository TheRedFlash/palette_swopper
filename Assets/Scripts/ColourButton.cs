﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourButton : MonoBehaviour {

    public bool buttonActivated;

    PlayerColour _playerColour;

    //All the basic actions needed before we expand on the code for more specific purposes
	public virtual void Start () {
        _playerColour = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerColour>();
        if (Array.Exists(_playerColour.availableColours, element => element == tag))
            GetComponent<Renderer>().materials[1].mainTexture = Resources.Load<Texture>("ColourTextures/" + tag + "body_albedo");
        //rend.material = Resources.Load<Material>("Colours/" + tag);
        else
            Debug.LogWarning("The following button is missing a colour tag - " + name);
        buttonActivated = false;
	}

    //We use a switch statement as different collisions will have different script references to get their colour
    public virtual void OnTriggerEnter(Collider col)
    {
        switch (col.tag)
        {
            case "Player":
                if (_playerColour.currentColour == tag)
                {
                    buttonActivated = true;
                }
                break;
            case "Sponge":
                BlockColour _blockColour = col.GetComponent<BlockColour>();
                if (_blockColour.currentColour == tag)
                {
                    buttonActivated = true;
                }
                break;
            case "Block":
                NoChangeBlockColour _noChangeBlockColour = col.GetComponent<NoChangeBlockColour>();
                if (_noChangeBlockColour.currentColour == tag)
                {
                    buttonActivated = true;
                }
                break;
        }
    }

    public virtual void OnTriggerExit(Collider col)
    {
        switch (col.tag)
        {
            case "Player":
                PlayerColour _playerColour = col.GetComponent<PlayerColour>();
                if (_playerColour.currentColour == tag)
                {
                    buttonActivated = false;
                }
                break;
            case "Sponge":
                BlockColour _blockColour = col.GetComponent<BlockColour>();
                if (_blockColour.currentColour == tag)
                {
                    buttonActivated = false;
                }
                break;
            case "Block":
                NoChangeBlockColour _noChangeBlockColour = col.GetComponent<NoChangeBlockColour>();
                if (_noChangeBlockColour.currentColour == tag)
                {
                    buttonActivated = false;
                }
                break;
        }
    }
}
