﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockWater : MonoBehaviour {

    PlayerColour _playerColour;

    bool waterOn = true;

    public ParticleSystem _particleSystem;
    public ParticleSystem.MainModule psMain;

	void Start ()
    {
        _playerColour = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerColour>();
        psMain = _particleSystem.main;
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && waterOn)
        {
            _playerColour.ChangeColour("Grey");
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Block")
        {
            waterOn = false;
            psMain.startLifetime = 0.4f;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Block")
        {
            waterOn = true;
            psMain.startLifetime = 2;
        }
    }
}
