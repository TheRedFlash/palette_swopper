﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    List<AudioSource> pausedSources = new List<AudioSource>();
    public bool gamePaused = false;
    public GameObject pauseMenu;
    public GameObject quitConfirmation;
    AudioManager _audioManager;

	// Use this for initialization
	void Start () {
        _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();

    }

    // Update is called once per frame
    void Update () {

        Time.timeScale = (!gamePaused) ? 1.00f : 0.00f;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
            TriggerPauseMenu();
        }    
    }

    public void PauseGame()
    {
        PauseSound();
        gamePaused = !gamePaused;   
    }

    public void PauseSound()
    {
        if (!gamePaused)
        {

            foreach (AudioSource s in GameObject.FindObjectsOfType<AudioSource>())
            {
                if (s.isPlaying)
                    s.Pause();
                pausedSources.Add(s);
            }
            _audioManager.Play("EnterPause");

        }
        else
        {

            foreach (AudioSource s in pausedSources.ToArray())
            {
                s.UnPause();
                pausedSources.Remove(s);
            }
            _audioManager.Play("ExitPause");
        }
    }

    public void TriggerPauseMenu()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
    }

    public void ResetLevel()
    {
        PauseGame();
        TriggerPauseMenu();
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.buildIndex);
    }

    public void LoadMenu()
    {
        FindObjectOfType<PauseMenu>().PauseGame();
        GameObject.Find("InGameMenuCanvas").transform.GetChild(0).gameObject.SetActive(false);
        SceneManager.LoadScene(2);
    }

    public void LoadMenuFromPause()
    {
        FindObjectOfType<PauseMenu>().PauseGame();
        TriggerPauseMenu();
        SceneManager.LoadScene(2);
    }

    public void LevelClearedNextLevel()
    {
        Scene scene = SceneManager.GetActiveScene();
        FindObjectOfType<PauseMenu>().PauseGame();
        pauseMenu.SetActive(false);
        SceneManager.LoadScene(scene.buildIndex + 1);
    }

    public void CancelQuit()
    {
        quitConfirmation.SetActive(false);
    }

    public void QuitGame(int quitStage)
    {
        if(quitStage == 0)
        {
            quitConfirmation.SetActive(true);
        }
        else
        {
            Debug.Log("Quit");
            Application.Quit();
        }
    }
}
