﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourPool : MonoBehaviour {

    string poolColour;
    PlayerColour _playerColour;

	void Start () {
        _playerColour = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerColour>();
        if (Array.Exists(_playerColour.availableColours, element => element == tag))
        {
            poolColour = tag;
            GetComponent<Renderer>().material = Resources.Load<Material>("Colours/" + poolColour);
        }
        else if (tag == "Water")
            poolColour = "Grey";
        else
            Debug.LogWarning("Pool - " + name + " has no tag for it's colour! It's current tag is - " + tag);
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Sponge")
        {
            BlockColour _blockColour = collider.gameObject.GetComponent<BlockColour>();
            if (_blockColour.currentColour == "Grey")
                _blockColour.ChangeColour(poolColour);

            if (poolColour != _blockColour.currentColour)
            {
                _blockColour.ChangeColour(poolColour);
            }
        }
        
        if (collider.gameObject.tag == "Player")
        {
            PlayerColour _playerColour = collider.gameObject.GetComponent<PlayerColour>();
            if (_playerColour.currentColour == "Grey")
                _playerColour.ChangeColour(poolColour);

            if (_playerColour.currentColour != poolColour)
            {
                _playerColour.ChangeColour(poolColour);
            }

        }
    }
}
