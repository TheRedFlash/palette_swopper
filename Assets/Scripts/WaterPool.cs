﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPool : MonoBehaviour {

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            PlayerColour _playerColour = col.gameObject.GetComponent<PlayerColour>();
            _playerColour.currentColour = "Grey";
            _playerColour.ChangeMaterial();
        }
    }
}
