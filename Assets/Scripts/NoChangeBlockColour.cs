﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoChangeBlockColour : BlockColour {

    public string staticBlockColour;

    public override void Start()
    {
        rend = GetComponent<Renderer>();
        if (staticBlockColour != "")
        {
            currentColour = staticBlockColour;
            ChangeMaterial();
        }
        else
            Debug.LogWarning("The following block has not been given a colour! " + name);
    }
}
