﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockReset : MonoBehaviour {

    public List<GameObject> blocks = new List<GameObject>();

    public List<Vector3> blockPositions = new List<Vector3>();

	private void Start ()
    {
        //Finds all the blocks in the scene, adds them to a list and then makes a list of all their inital positions
        foreach (GameObject block in GameObject.FindGameObjectsWithTag("Block"))
        {
            blocks.Add(block);
            blockPositions.Add(block.transform.position);
        }
        foreach (GameObject sponge in GameObject.FindGameObjectsWithTag("Sponge"))
        {
            blocks.Add(sponge);
            blockPositions.Add(sponge.transform.position);
        }
	}


    //When the trigger is entered it sets each block's position back to it's starting position and resets their colour
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            int i = 0;
            foreach (GameObject block in blocks)
            {
                block.transform.position = blockPositions[i];
                i++;
                block.GetComponent<BlockColour>().currentColour = block.GetComponent<BlockColour>().startingColour;
                block.GetComponent<BlockColour>().ChangeMaterial();
            }
            if (GameObject.FindGameObjectsWithTag("RaisePlatform") != null)
            {
                foreach (GameObject platform in GameObject.FindGameObjectsWithTag("RaisePlatform"))
                {
                    platform.GetComponent<Animator>().SetBool("raiseButtonPressed", false);
                }
            }
        }
    }
}
