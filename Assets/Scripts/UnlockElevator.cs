﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockElevator : ColourButton {

    public GameObject elevator;
    public GameObject elevatorDoor;
    public AudioManager _audioManager;

    public override void Start () {
        base.Start();
        elevator = GameObject.Find("Elevator");
        elevatorDoor = GameObject.Find("ElevatorDoor");
        elevator.GetComponent<Collider>().isTrigger = false;
        _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    }

    public override void OnTriggerEnter(Collider col)
    {
        base.OnTriggerEnter(col);
        if (buttonActivated == true)
        {
            elevatorDoor.SetActive(false);
            elevator.GetComponent<Collider>().isTrigger = true;
            _audioManager.Play("ElevatorOpen");
        }
    }
}
