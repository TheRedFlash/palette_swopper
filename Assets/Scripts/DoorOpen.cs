﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : ColourButton {

    public GameObject doorToOpen;
    Animator anim;

    public override void Start()
    {
        base.Start();
        if (doorToOpen == null)
            Debug.LogWarning("Button: " + name + " is missing which door it is meant to open");
        anim = doorToOpen.GetComponent<Animator>();
    }

    public override void OnTriggerEnter(Collider col)
    {
        base.OnTriggerEnter(col);
        if (buttonActivated == true)
            anim.SetTrigger("DoorOpen");
    }
}
