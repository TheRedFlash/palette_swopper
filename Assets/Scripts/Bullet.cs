﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    [SerializeField]
    private float timer = 0f;
    private float bulletSpeed;
    private float bulletLifeTime;
    private bool timerSet;

	void Awake ()
    {
        timerSet = false;
	}
	
	void Update () {

        if(timerSet == false)
        {
            timer = bulletLifeTime;
            timerSet = true;
        }

        timer = timer - Time.deltaTime;

        if (timer <= 0.0f)
            Destroy(gameObject);

        transform.Translate(Vector3.forward * Time.deltaTime * bulletSpeed);
    }

    public void SetBulletSpeed(float speed)
    {
        bulletSpeed = speed;
    }

    public void SetBulletLifetime(float time)
    {
        bulletLifeTime = time;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerColour _playerColour = other.GetComponent<PlayerColour>();
            _playerColour.ChangeColour(tag);
            Destroy(gameObject);
        }
        if (other.gameObject.tag == "Sponge")
        {
            BlockColour _blockColour = other.GetComponent<BlockColour>();
            _blockColour.ChangeColour(tag);
            Destroy(gameObject);
        }
        if (other.gameObject.tag == "Block")
        {
            Destroy(gameObject);
        }
    }
}
