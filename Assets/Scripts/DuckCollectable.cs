﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckCollectable : MonoBehaviour {

    private DuckTracker _duckTracker;

    private void Awake()
    {
        _duckTracker = GameObject.Find("DuckTracker").GetComponent<DuckTracker>();
    }

    private void OnEnable()
    {
        if (_duckTracker.duckGOsCollected.Contains(gameObject.name))
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("DuckGet");
            if(!_duckTracker.duckGOsCollected.Contains(gameObject.name))
                _duckTracker.duckGOsCollected.Add(gameObject.name);
            Destroy(gameObject);
        }
    }
}
