﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour {

    PlayerColour _playerColour;

    AudioManager _audioManger;

    private void OnEnable()
    {
        GameObject.Find("InGameMenuCanvas").transform.GetChild(0).gameObject.SetActive(false);
    }

    void Start ()
    {
        _playerColour = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerColour>();
        _audioManger = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        if (Array.Exists(_playerColour.availableColours, element => element == tag))
        {
            transform.GetChild(0).GetComponent<Renderer>().materials[2].mainTexture = Resources.Load<Texture>("ColourTextures/" + tag + "body_albedo");
        }
        else
            Debug.LogWarning("The Elevator needs a colour tag!");
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player" && _playerColour.currentColour == tag)
        {

            _audioManger.Play("LevelComplete");
            GameObject.Find("InGameMenuCanvas").transform.GetChild(1).GetComponent<PauseMenu>().PauseSound();
            GameObject.Find("InGameMenuCanvas").transform.GetChild(1).GetComponent<PauseMenu>().gamePaused = true;           
            GameObject.Find("InGameMenuCanvas").transform.GetChild(0).gameObject.SetActive(true);
        }
    }
}
