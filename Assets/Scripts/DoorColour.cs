﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorColour : MonoBehaviour {

    Renderer rend;

    void Start()
    {
        rend = GameObject.Find("Gate").GetComponent<Renderer>();
        rend.material = Resources.Load<Material>("Colours/" + tag);
    }
}
