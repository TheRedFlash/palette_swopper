﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockElevatorLv2 : UnlockElevator {

    public override void Start () {
        base.Start();
    }

    public override void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Sponge")
        {
            if (col.GetComponent<BlockColour>().currentColour == tag)
            {
                elevatorDoor.SetActive(false);
                elevator.GetComponent<Collider>().isTrigger = true;
                _audioManager.Play("ElevatorOpen");
            }
        }
    }
}
