﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuImageFade : MonoBehaviour {

    public Image[] art;
    public Image artA;
    public Image artB;
    public int artIndex;
    public int artIndexB;
    public float artTimer;

	void Start () {
        artIndex = 0;
        artTimer = 5;
        artIndexB = artIndex + 1;
        foreach (Image art in art)
            art.canvasRenderer.SetAlpha(0f);
        art[0].canvasRenderer.SetAlpha(1);
    }
	
	void Update () {
        if (Input.GetKeyDown(KeyCode.O))
        {
            FadeBetweenArt(art[artIndex], art[artIndexB]);
        }

        artTimer -= Time.deltaTime;

        if (artTimer <= 0)
        {
            FadeBetweenArt(art[artIndex], art[artIndexB]);
            artTimer = 6.5f;
        }       
	}

    void FadeBetweenArt(Image a, Image b)
    {
        a.canvasRenderer.SetAlpha(1f);
        b.canvasRenderer.SetAlpha(0f);


        a.CrossFadeAlpha(0, 1.5f, false);
        b.CrossFadeAlpha(1, 1.5f, false);
        artIndex++;
        artIndexB++;
        if (artIndex == 4)
            artIndex = 0;
        if (artIndexB == 4)
            artIndexB = 0;
    }
}
