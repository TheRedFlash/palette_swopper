﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreenText : MonoBehaviour {

    public Text duckText;
    public Image duck;
    DuckTracker _duckTracker;
    int ducksCollected;

    private void OnEnable()
    {
        _duckTracker = GameObject.Find("DuckTracker").GetComponent<DuckTracker>();
        ducksCollected = _duckTracker.duckGOsCollected.Count;
        if (ducksCollected != _duckTracker.totalDucks)
            duckText.text = "Congratulations!\nYou collected " + ducksCollected + "/" + _duckTracker.totalDucks + " Ducks\nGo back and try to find them all!";
        else
            duckText.text = "Congratulations!\nYou collected all the hidden rubber ducks!";
        switch (ducksCollected)
        {
            case 0:
                duck.sprite = Resources.Load<Sprite>("duck_none");
                break;
            case 4:
                duck.sprite = Resources.Load<Sprite>("duck_all");
                break;
            default:
                duck.sprite = Resources.Load<Sprite>("duck_some");
                break;
        }
    }
}
