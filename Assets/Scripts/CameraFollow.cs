﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // The Camera Target
    [SerializeField]
    Transform m_PlayerTransform;

    // The Z Distance from the Camera Target
    [SerializeField]
    float m_CameraDistanceZ;

    // The Y Distance from the Camera Target
    [SerializeField]
    float m_CameraDistanceY;

    void Start()
    {
        m_PlayerTransform = GameObject.FindGameObjectWithTag("Player").gameObject.transform;
    }

    void Update()
    {
        transform.position = new Vector3(m_PlayerTransform.position.x, m_PlayerTransform.position.y + m_CameraDistanceY, m_PlayerTransform.position.z - m_CameraDistanceZ);
    }
}