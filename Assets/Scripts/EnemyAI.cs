﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {

    public GameObject shootPoint;
    public GameObject bulletObject;

    [Tooltip("Speed at which enemy shoots, max is 4")]
    public float shootTimeMultiplier;

    private float shootTimer;
    private float shootDelay;

    private float animationTimer;
    private float animationDelay;

    [Tooltip("Speed at which bullet travels")]
    public float bulletSpeed;

    public float bulletLifetime;

    bool bulletShot;

    public string shootMaterial;

    public Animator anim;

    private string[] inkColours = new string[7] { "Blue", "Yellow", "Red", "Green", "Orange", "Purple", "Brown" };

    void Start () {
        anim = GetComponent<Animator>();
        shootPoint = transform.Find("ShootPoint").gameObject;
        bulletObject = Resources.Load<GameObject>("Prefabs/Bullet");
        if (Array.Exists(inkColours, element => element == tag))
            shootMaterial = "Ink";
        else if (tag == "Water" || tag == "Grey")
            shootMaterial = "Water";
        else
            Debug.LogWarning("Enemy : " + name + "is missing a valid tag");

        shootDelay = 2.0f;
        shootDelay = shootDelay / shootTimeMultiplier;
        animationDelay = shootDelay / 2;
        anim.SetFloat("ShootSpeed", 4.5f * shootTimeMultiplier);
	}
	
	void Update () {
        //The Following code is made so that when the enemy finishes it's animation the bullet is shot and then a delay the length of the animation is made
        if (shootTimer <= 0.0f)
        {
            shootTimer = shootDelay;
        }
        if (shootTimer == shootDelay)
        {
            anim.SetTrigger("Shoot");
            animationTimer = animationDelay;
            bulletShot = false;
        }
        if (animationTimer <= 0f && bulletShot == false)
        {
            ShootBullet(shootMaterial);
            bulletShot = true;
        }
        
        if (shootTimer > 0)
            shootTimer = shootTimer - Time.deltaTime;
        if(animationTimer>0)
            animationTimer = animationTimer - Time.deltaTime;
	}

    void ShootBullet(string type)
    {
        switch (type)
        {
            case "Ink":
                GameObject bulletGO = Instantiate(bulletObject, shootPoint.transform.position, shootPoint.transform.rotation) as GameObject;
                bulletGO.transform.parent = gameObject.transform;
                bulletGO.tag = gameObject.tag;
                bulletGO.GetComponent<Bullet>().SetBulletSpeed(bulletSpeed);
                bulletGO.GetComponent<Bullet>().SetBulletLifetime(bulletLifetime);
                bulletGO.GetComponent<Renderer>().sharedMaterial = Resources.Load<Material>("Colours/" + bulletGO.tag);
                break;
            case "Water":
                bulletGO = Instantiate(bulletObject, shootPoint.transform.position, shootPoint.transform.rotation) as GameObject;
                bulletGO.transform.parent = gameObject.transform;
                bulletGO.tag = "Grey";
                bulletGO.GetComponent<Bullet>().SetBulletSpeed(bulletSpeed);
                bulletGO.GetComponent<Bullet>().SetBulletLifetime(bulletLifetime);
                bulletGO.GetComponent<Renderer>().sharedMaterial = Resources.Load<Material>("Colours/" + bulletGO.tag);
                break;
            default:
                Debug.LogWarning("No type supplied on - " + name);
                break;
        }
        anim.ResetTrigger("Shoot");
    }
}
