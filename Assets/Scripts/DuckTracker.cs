﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckTracker : MonoBehaviour {

    public int totalDucks = 4;
    public List<string> duckGOsCollected = new List<string>();

	void Start ()
    {
        if (GameObject.Find(gameObject.name)
                 && GameObject.Find(gameObject.name) != gameObject)
        {
            Destroy(GameObject.Find(gameObject.name));
        }

        totalDucks = 4;
        foreach (string duck in duckGOsCollected.ToArray())
        {
            duckGOsCollected.Remove(duck);
        }
        
        DontDestroyOnLoad(gameObject);      
    }
}
