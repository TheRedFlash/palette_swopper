﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class FallingWaterSound : MonoBehaviour {

    private AudioManager _audioManager;

    private void Awake()
    {
        _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        SceneManager.sceneUnloaded += OnSceneUnload;
    }

    void Start () {
        _audioManager.Play("FallingWater");
	}
	
    void OnSceneUnload<Scene> (Scene scene)
    {
        _audioManager.Stop("FallingWater");
    }
}
