﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerColour : MonoBehaviour {

    public string currentColour;
    [SerializeField]
    public string[] availableColours;
    public Renderer rend;
    private AudioManager _audioManager;

    private void Awake()
    {
        //Set an array with all colours available for checking valid tags 
        availableColours = new string[8] { "Blue", "Yellow", "Red", "Green", "Orange", "Purple", "Grey", "Brown" };
        _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    }

    public virtual void Start()
    {
        rend = GameObject.Find("PolySphere").GetComponent<Renderer>();
        
        currentColour = "Grey";
        ChangeMaterial();
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == "Water")
        {
            ChangeColour("Grey");
            transform.GetChild(0).GetComponent<PlayerAnimationController>().stepWater = true;
        }
        else
            transform.GetChild(0).GetComponent<PlayerAnimationController>().stepWater = false;
    }

    public void ChangeMaterial()
    {
        if (tag == "Player")
        {
            rend.materials[2].mainTexture = Resources.Load<Texture>("ColourTextures/" + currentColour + "ring_albedo");
            rend.materials[4].mainTexture = Resources.Load<Texture>("ColourTextures/" + currentColour + "ring_albedo");
            rend.materials[0].mainTexture = Resources.Load<Texture>("ColourTextures/" + currentColour + "body_albedo");
            rend.materials[3].mainTexture = Resources.Load<Texture>("ColourTextures/" + currentColour + "body_albedo");
            transform.GetChild(0).GetComponent<PlayerAnimationController>().ColourChange();
            return;
        }

        if (tag == "Sponge" || tag == "Block")
        {
            if (Array.Exists(availableColours, element => element == currentColour))
            {
                switch (currentColour)
                {
                    case "Grey":
                        rend.material.SetColor("_Color", Color.grey);
                        break;
                    case "Blue":
                        rend.material.color = Color.blue;
                        break;
                    case "Red":
                        rend.material.color = Color.red;
                        break;
                    case "Yellow":
                        rend.material.color = Color.yellow;
                        break;
                    case "Green":
                        rend.material.color = Color.green;
                        break;
                    case "Orange":
                        rend.material.color = new Color(1, 0.556f, 0);
                        break;
                    case "Purple":
                        rend.material.color = Color.magenta;
                        break;
                    case "Brown":
                        rend.material.color = new Color(0.37f, 0.16f, 0);
                        break;
                }
            }
            _audioManager.Play("BlockColourChange");
            return;
        }

        if (Array.Exists(availableColours, element => element == tag))
        {
            if (currentColour == "Grey")
                rend.materials[1].mainTexture = Resources.Load<Texture>("ColourTextures/claberMinusEye");
            else
                rend.materials[1].mainTexture = Resources.Load<Texture>("ColourTextures/claberPlusEye");
            rend.materials[0].mainTexture = Resources.Load<Texture>("ColourTextures/" + currentColour + "claberBody");
            rend.materials[2].mainTexture = Resources.Load<Texture>("ColourTextures/" + currentColour + "claberBrowTail");
        }

    }
    
    public void ChangeColour(string incomingColour)
    {
        if (Array.Exists(availableColours, element => element == incomingColour))
        {
            if (incomingColour != currentColour)
            {
                switch (currentColour)
                {
                    case "Grey":
                        currentColour = incomingColour;
                        break;
                    case "Blue":
                        if (incomingColour == "Grey")
                            currentColour = incomingColour;
                        if (incomingColour == "Yellow")
                            currentColour = "Green";
                        if (incomingColour == "Red")
                            currentColour = "Purple";
                        break;
                    case "Red":
                        if (incomingColour == "Grey")
                            currentColour = incomingColour;
                        if (incomingColour == "Yellow")
                            currentColour = "Orange";
                        if (incomingColour == "Blue")
                            currentColour = "Purple";
                        break;
                    case "Yellow":
                        if (incomingColour == "Grey")
                            currentColour = incomingColour;
                        if (incomingColour == "Blue")
                            currentColour = "Green";
                        if (incomingColour == "Red")
                            currentColour = "Orange";
                        break;
                    case "Green":
                        if (incomingColour == "Grey")
                            currentColour = incomingColour;
                        else if (incomingColour != "Blue" && incomingColour != "Yellow")
                            currentColour = "Brown";
                        break;
                    case "Orange":
                        if (incomingColour == "Grey")
                            currentColour = incomingColour;
                        else if (incomingColour != "Red" && incomingColour != "Yellow")
                            currentColour = "Brown";
                        break;
                    case "Purple":
                        if (incomingColour == "Grey")
                            currentColour = incomingColour;
                        else if (incomingColour != "Blue" && incomingColour != "Red")
                            currentColour = "Brown";
                        break;
                    case "Brown":
                        if (incomingColour == "Grey")
                            currentColour = incomingColour;
                        break;
                }

                ChangeMaterial();
            }
        }
    }
}
