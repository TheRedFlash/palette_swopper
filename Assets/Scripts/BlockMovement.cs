﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMovement : MonoBehaviour {

    public float pushPower = 5.0f;
    public float grabRange = 2.0f;
    public bool blockGrabbed;
    public GameObject targetBlock;
    private PlayerController _playerController;
    private AudioManager _audioManager;

    private void Start ()
    {
        blockGrabbed = false;
        _playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        _audioManager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    }

    private void Update ()
    {
        //If the nearby block leaves range and there is a target block it releases said block
        if (CheckNearbyBlock() == null && targetBlock != null)
        {
            ReleaseBlock(targetBlock);
            return;
        }

        //If Left Mouse button is pressed while in range of a block it will grab the closest block
        if (Input.GetKey(KeyCode.Mouse0) && CheckNearbyBlock() != null)
            if (blockGrabbed == false)
                GrabBlock(CheckNearbyBlock());

        //If Left Mouse button is released it will let go of the grabbed block
        if (Input.GetKeyUp(KeyCode.Mouse0) && CheckNearbyBlock()!= null)
            if(blockGrabbed == true)
                ReleaseBlock(targetBlock);

        if (_playerController.playerInputDetected == true && blockGrabbed == true && !_audioManager.IsPlaying("BlockPush"))
            _audioManager.Play("BlockPush");
        
	}

    //OnControllerColliderHit is used as the character controller acts differently to a rigidbody controller
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;

        //If it is not pushing agaisnt anything it won't run the code
        if (body == null || body.isKinematic)
            return;

        //If the player is moving and there is a block grabbed it will move in the direction of the collision
        if (_playerController.playerInputDetected && blockGrabbed)
        {
            Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);
            body.velocity = pushDir * _playerController.m_RunSpeed * pushPower;
        }
    }

    //A function that finds all the blocks in the scene and finds the closest one in range and sets it to the target block
    private GameObject CheckNearbyBlock()
    {
        List<GameObject> blocksInScene = new List<GameObject>();

        GameObject nearestBlock = null;
        float distanceToNearestBlock = Mathf.Infinity;

        foreach(GameObject block in GameObject.FindGameObjectsWithTag("Sponge"))
            blocksInScene.Add(block);
        foreach (GameObject block in GameObject.FindGameObjectsWithTag("Block"))
            blocksInScene.Add(block);

        List<GameObject> blocksInGrabRange = new List<GameObject>();

        //Finds the blocks in grab range
        foreach(GameObject block in blocksInScene)
        {
            float distanceToBlock = Vector3.Distance(transform.position, block.transform.position);
            if (distanceToBlock <= grabRange)
                blocksInGrabRange.Add(block);
        }
        //Finds the closest one
        foreach(GameObject block in blocksInGrabRange)
        {
            float distanceToBlock = Vector3.Distance(transform.position, block.transform.position);
            if (distanceToBlock < distanceToNearestBlock)
            {
                distanceToNearestBlock = distanceToBlock;
                nearestBlock = block;
            }
        }

        //If there is a block in grab range it sets it as the target, if there is no block in range it returns null
        if (nearestBlock != null)
        {
            targetBlock = nearestBlock;
            return nearestBlock;
        }
        else
            return null;
    }

    void GrabBlock(GameObject block)
    {
        _playerController.playerAnimatonController.SetAnimatorParameters("BoxGrabbed", "true");
        block.GetComponent<Rigidbody>().isKinematic = false;
        blockGrabbed = true;   
    }

    void ReleaseBlock(GameObject block)
    {
        _playerController.playerAnimatonController.SetAnimatorParameters("BoxGrabbed", "false");
        block.GetComponent<Rigidbody>().isKinematic = true;
        blockGrabbed = false;
        targetBlock = null;
    }
}
