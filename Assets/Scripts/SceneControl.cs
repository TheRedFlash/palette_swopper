﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class SceneControl : MonoBehaviour {

    float timer = 0.5f;

    private void Start()
    {
        timer = 1;
    }

    private void Update()
    {
        if (GetCurrentSceneIndex() == 0)
            timer -= Time.deltaTime;
        if (GetCurrentSceneIndex() == 1 && Input.GetKeyDown(KeyCode.Mouse0))
            LoadScene(GetCurrentSceneIndex() + 1);
        if(GetCurrentSceneIndex() == 0)
            if (!FindObjectOfType<VideoPlayer>().GetComponent<VideoPlayer>().isPlaying && timer <= 0)
                LoadScene(GetCurrentSceneIndex() + 1);
        if(GetCurrentSceneIndex() == 7)
            GameObject.Find("InGameMenuCanvas").transform.GetChild(0).gameObject.SetActive(false);
    }

    public void LoadScene(int i)
	{
		SceneManager.LoadScene (i);
	}

    public int GetCurrentSceneIndex()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }

	public void QuitGame()
	{
		Application.Quit();
	}
}
