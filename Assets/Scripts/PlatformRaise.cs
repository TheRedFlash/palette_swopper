﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformRaise : ColourButton {

    public GameObject raisablePlatform;
    Animator anim;

    public override void Start()
    {
        base.Start();
        if (raisablePlatform == null)
            Debug.LogWarning("RaisablePlatform reference missing from " + name);
        anim = raisablePlatform.GetComponent<Animator>();
    }

    public override void OnTriggerEnter(Collider col)
    {
        base.OnTriggerEnter(col);
        if (buttonActivated == true)
            anim.SetBool("raiseButtonPressed", true);
    }

    public override void OnTriggerExit(Collider other)
    {
        base.OnTriggerExit(other);
        if (buttonActivated == false)
            anim.SetBool("raiseButtonPressed", false);
    }
}
