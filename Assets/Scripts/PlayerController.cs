﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    // --------------------------------------------------------------

    // The character's running speed
    [SerializeField]
    public float m_RunSpeed = 5.0f;

    // The gravity strength
    [SerializeField]
    float m_Gravity = 60.0f;

    // The maximum speed the character can fall
    [SerializeField]
    float m_MaxFallSpeed = 20.0f;

    // The character's jump height
    [SerializeField]
    float m_JumpHeight = 4.0f;

    // --------------------------------------------------------------

    // The charactercontroller of the player
    CharacterController m_CharacterController;

    // The current movement direction in x & z.
    Vector3 m_MovementDirection = Vector3.zero;

    // The current movement speed
    float m_MovementSpeed = 0.0f;

    // The current vertical / falling speed
    float m_VerticalSpeed = 0.0f;

    // The current movement offset
    Vector3 m_CurrentMovementOffset = Vector3.zero;

    // The force added to the player (used for knockbacks)
    Vector3 m_Force = Vector3.zero;

    //Is the player moving
    public bool playerInputDetected;

    public PlayerAnimationController playerAnimatonController;

    // --------------------------------------------------------------

    void Awake()
    {
        m_CharacterController = GetComponent<CharacterController>();      
        playerAnimatonController = transform.GetChild(0).GetComponent<PlayerAnimationController>();
    }

    void Update()
    {
        if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
            playerInputDetected = true;
        else
        {        
            playerInputDetected = false;
        }

        if (m_CharacterController.isGrounded && playerAnimatonController.anim.GetBool("isJumping"))
        {
            playerAnimatonController.Land();
            playerAnimatonController.SetAnimatorParameters("isJumping", "false");
        }

        // Update movement input
        UpdateMovementState();

        //Update jumping input and apply gravity
        UpdateJumpState();
        if(!m_CharacterController.isGrounded)
            ApplyGravity();

        // Calculate actual motion
        m_CurrentMovementOffset = (m_MovementDirection * m_MovementSpeed + m_Force + new Vector3(0, m_VerticalSpeed, 0)) * Time.deltaTime;

        m_Force *= 0.95f;

        // Move character
        m_CharacterController.Move(m_CurrentMovementOffset);
        if (playerInputDetected)
        {
            playerAnimatonController.SetAnimatorParameters("isWalking", "true");
            playerAnimatonController.idleTimer = playerAnimatonController.idleDelay;
            playerAnimatonController.idleCountdown = false;
        }

        if (playerInputDetected == false)
            playerAnimatonController.SetAnimatorParameters("isWalking", "false");

        // Rotate the character towards direction of movement
        RotateCharacter(m_MovementDirection);

    }

    void UpdateJumpState()
    {
        // Character can jump when standing on the ground
        if (Input.GetButtonDown("Jump") && m_CharacterController.isGrounded)
        {
            playerAnimatonController.SetAnimatorParameters("isJumping", "true");
            playerAnimatonController.Jump();
            Jump();
        }
    }

    void Jump()
    {

        m_VerticalSpeed = Mathf.Sqrt(m_JumpHeight * m_Gravity);
    }

    void ApplyGravity()
    {
        // Apply gravity
        m_VerticalSpeed -= m_Gravity * Time.deltaTime;

        // Make sure we don't fall any faster than m_MaxFallSpeed.
        m_VerticalSpeed = Mathf.Max(m_VerticalSpeed, -m_MaxFallSpeed);
        m_VerticalSpeed = Mathf.Min(m_VerticalSpeed, m_MaxFallSpeed);
    } 

    void UpdateMovementState()
    {
        // Get Player's movement input and determine direction and set run speed
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");
        m_MovementDirection = new Vector3(horizontalInput, 0, verticalInput);
        m_MovementSpeed = m_RunSpeed;
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    void RotateCharacter(Vector3 movementDirection)
    {
        Quaternion lookRotation = Quaternion.LookRotation(movementDirection);
        if (transform.rotation != lookRotation && playerInputDetected)
        {
            transform.rotation = lookRotation;
        }
    }


    void AddForce(Vector3 force)
    {
        m_Force += force;
    }

}
